#ifndef __ANIMATIONS_H__
#define __ANIMATIONS_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <dirent.h>
#include <string.h>

#include <animod.h>

uint8_t loadAnimations (char * anidir);
void releaseAnimations (void);

#endif