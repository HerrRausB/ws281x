#include "animations.h"

// internal stuff

uint8_t	_animodcnt = 0;
animation_module_t * _animods = NULL;

int _animodFilter(const struct dirent *entry);

int _animodFilter(const struct dirent *entry)
{
	int retval = 0, 
		extlen = strlen(DEF_ANIMOD_EXTENSION), 
		namelen = strlen(entry->d_name);

	// if the found file is a regular file and its name is 
	// longer then the standard animation file extension then
	// it might be an animation plugin, so...
	if ((entry->d_type == DT_REG) && (namelen > extlen))
	{
		// ... add this file to the filter list, if its extension
		// matches the standard animation extension
		retval = !strcmp (entry->d_name+(namelen-extlen), DEF_ANIMOD_EXTENSION);
	}
	return retval;
}

// API

uint8_t loadAnimations(char * anidir)
{
	struct dirent **namelist;
	int i, n;
	char * animodfile = NULL;

	animation_module_t 	* panimod = NULL,
						* piter = NULL;


	if ((n = scandir(anidir, &namelist, _animodFilter, alphasort)) >= 0)
	{
		for (_animodcnt = 0, i = 0; i < n; i++)
		{
			if ((panimod = (animation_module_t *)malloc(sizeof(animation_module_t))) &&
				(animodfile = malloc(sizeof(char) * (strlen(anidir) + strlen(namelist[i]->d_name)+2))))
			{
				memset (panimod, 0, sizeof (animation_module_t));
				sprintf (animodfile, "%s/%s", anidir, namelist[i]->d_name);
				fprintf (stderr, "loading %s\n", animodfile);
				if 	(panimod->hmod = dlopen(animodfile, RTLD_LAZY))
				{
					if 	(
						(panimod->doInit = dlsym (panimod->hmod, "doInit")) &&
						(panimod->getName = dlsym (panimod->hmod, "getName")) &&
						(panimod->getDesc = dlsym (panimod->hmod, "getDesc")) &&
						(panimod->doStart = dlsym (panimod->hmod, "doStart")) &&
						(panimod->doNext = dlsym (panimod->hmod, "doNext")) &&
						(panimod->doRandomAction = dlsym (panimod->hmod, "doRandomAction")) &&
						(panimod->doExtraAction = dlsym (panimod->hmod, "doExtraAction")) &&
						(panimod->doStop = dlsym (panimod->hmod, "doStop")) &&
						(panimod->doTerminate = dlsym (panimod->hmod, "doTerminate"))
						)
					{
						if (!_animodcnt) 
						{
							_animods = panimod;
						}
						else
						{
							piter->next = panimod;
						}
						piter = panimod;

						_animodcnt++;
					}
					else
					{
						fprintf (stderr, "allocating functions: %s\n", dlerror());
						dlclose (panimod->hmod);
					}
				}
				else
				{
					fprintf (stderr, "loading module %s: %s\n", namelist[i]->d_name, dlerror());
					free (panimod);
				}
				free (animodfile);
			}
			free(namelist[i]);
		}
		free(namelist);
	}
	return _animodcnt;
}

void releaseAnimations ()
{
	animation_module_t * panimod = _animods;

	while (panimod)
	{
		if (panimod->hmod)
		{
			dlclose (panimod->hmod);
		}
		panimod = _animods->next;
		free (_animods);
		_animods = panimod;
	}
}