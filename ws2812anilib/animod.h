#ifndef __ANIMOD_H__
#define __ANIMOD_H__

#include <stdint.h>
#include <ws2811.h>

#ifndef bool_t
	#define bool_t uint8_t
	#define true 1
	#define false 0
	#define TRUE 1
	#define FLASE 0
	#define DO_RENDER 1
	#define NO_RENDER 0
#endif

#define DEF_ANIMOD_EXTENSION	".animod"
#define DEF_ANIMOD_DIRECTORY	"./ws2812animods"

typedef struct _animod
{
	void * hmod;
	bool_t (*doInit)(ws2811_led_t *, int);
	char * (*getName)(void);
	char * (*getDesc)(void);
	bool_t (*doStart)(void);
	bool_t (*doNext)(void);
	bool_t (*doRandomAction)(void);
	bool_t (*doExtraAction)(void);
	bool_t (*doStop)(void);
	bool_t (*doTerminate)(void);
	struct _animod * next;
} 
animation_module_t;

bool_t doInit (ws2811_led_t *leds, int ledcount);
char * getName (void);
char * getDesc (void);
bool_t doStart (void);
bool_t doNext (void);
bool_t doRandomAction (void);
bool_t doExtraAction (void);
bool_t doStop (void);
bool_t doTerminate (void);

#endif
