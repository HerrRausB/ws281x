# general stuff

CURRDIR = $(shell pwd)

# test program

TARGET=ws2812
TARGETDIR = /usr/local/bin

CSRC = $(wildcard *.c)
COBJ = $(patsubst %.c, %.o, $(CSRC))

# ws2811 library

WS2811LIB = ws2811
WS2811SRCDIR = $(WS2811LIB)lib
WS2811TARGET = lib$(WS2811LIB).so
WS2811LIBDIR = /usr/local/lib
WS2811HEAD = $(wildcard $(WS2811SRCDIR)/*.h) 
WS2811HEADDIR = /usr/local/include

WS2811SRC = $(wildcard $(WS2811SRCDIR)/*.c)
WS2811OBJ = $(patsubst %.c, %.o, $(WS2811SRC))

# animation modules

ANIMODEXT = animod
ANIMODSDIR = $(TARGET)animods
ANIMODSRCDIR = ./$(ANIMODSDIR)
ANIMODSTARGETDIR = $(TARGETDIR)/$(ANIMODSDIR)

ANISRC = $(wildcard $(ANIMODSRCDIR)/*.c)               
ANIMODS = $(patsubst %.c, %.$(ANIMODEXT), $(ANISRC)) 

# compiler definitions

CC = gcc
CCFLAGS = -I $(WS2811SRCDIR) -I $(ANIMODSDIR)

# linker definitions

LD = gcc
LDFLAGS = 
#LIBS = -lpigpio -lrt
LIBS = -lc -lrt -ldl -L. -l$(WS2811LIB)

# how to compile plain C

%.o: %.c
	$(CC) -c -o $@ $< $(CCFLAGS)

# how to compile animation modules

%.$(ANIMODEXT): %.c
	$(LD) $(LDFLAGS) -shared -o $@ $< $(CCFLAGS)

# labels

$(WS2811TARGET): $(WS2811OBJ)
	$(LD) $(LDFLAGS) -shared -o $@ $^

$(TARGET): $(COBJ)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS) 

info:
	@echo "ws2811 lib sources: " $(WS2811SRC)
	@echo "ws2811 lib: " $(WS)
	@echo "animation sources: " $(ANISRC)
	@echo "animation modules: " $(ANIMODS)

clean:
	rm $(COBJ) $(WS2811OBJ) $(TARGET) $(WS2811TARGET) $(ANIMODS)

ws2811lib: $(WS2811TARGET)

mods: $(ANIMODS)

pgm: $(TARGET)

all: ws2811lib pgm mods

instws2811lib: ws2811lib
	sudo cp -a $(WS2811HEAD) $(WS2811HEADDIR)
	sudo cp -a $(WS2811TARGET) $(WS2811LIBDIR)
	sudo chown root:gpio $(WS2811LIBDIR)/$(WS2811TARGET)
	sudo chmod u=rwxs,g=rxs,o=rxs $(WS2811LIBDIR)/$(WS2811TARGET)
	sudo ldconfig

instpgm: pgm
	sudo cp -a $(TARGET) $(TARGETDIR)
	sudo chown root:gpio $(TARGETDIR)/$(TARGET)
	sudo chmod u=rwxs,g=rxs,o=rxs $(TARGETDIR)/$(TARGET)

instmods: mods
	@if [ ! -d "$(ANIMODSTARGETDIR)" ]; \
 	then \
		sudo mkdir $(ANIMODSTARGETDIR); \
	fi
	sudo cp -a $(ANIMODS) $(ANIMODSTARGETDIR)
	cd $(ANIMODSTARGETDIR)
	sudo chown root:gpio $(ANIMODS)
	sudo chmod u=rwxs,g=rxs,o=rxs $(ANIMODS)
	cd $(CURRDIR)

install:  instws2811lib instpgm instmods

docker: $(TARGET)
	- docker rm -f $(TARGET)
	docker build -t $(TARGET) .
	docker run -d -p 8888:8888 --privileged --name $(TARGET) $(TARGET)

run: install
	$(TARGET)
	
