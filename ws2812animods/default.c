#include <stdlib.h>

#include "animod.h"

// internal stuff

ws2811_led_t * _leds = NULL;
int _ledcount = 0;

bool_t doInit (ws2811_led_t *leds, int ledcount)
{
	bool_t retval = NO_RENDER;

	if ((ledcount > 0) && leds)
	{
		_leds = leds;
		_ledcount = ledcount;

		for (int i = 0; i < _ledcount; i++)
		{
			_leds[i] = 0x880088;
		}
		retval = DO_RENDER;
	}
	return retval;
}

char * getName (void)
{
	return "Default animation";
}

char * getDesc (void)
{
	return "Slowly moving rainbow spectrum";
}

bool_t doStart (void)
{
	return NO_RENDER;
}

bool_t doNext (void)
{
	return NO_RENDER;
}

bool_t doRandomAction (void)
{
	return NO_RENDER;
}

bool_t doExtraAction (void)
{
	return NO_RENDER;
}

bool_t doStop (void)
{
	return NO_RENDER;
}

bool_t doTerminate (void)
{
	bool_t retval = NO_RENDER;

	if ((_ledcount > 0) && _leds)
	{
		for (int i = 0; i < _ledcount; i++)
		{
			_leds[i] = 0x000000;
		}

		_ledcount = 0;
		_leds = NULL;

		retval = DO_RENDER;
	}
	return retval;
}

