import argparse
import os
import signal
import json
import socket
import time
import random
from queue import Queue
import threading
import math
import paho.mqtt.client as mqttc
from rpi_ws281x import PixelStrip, Color

# LED strip configuration:
LED_PIN_1 = 12          # GPIO pin connected to the pixels (18 uses PWM!).
LED_PIN_2 = 13          # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /d ev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
# True to invert the signal (when using NPN transistor level shift)
LED_INVERT = False
LED_CHN_1 = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53
LED_CHN_2 = 1       # set to '1' for GPIOs 13, 19, 41, 45 or 53

C_GRN = Color(0, 255, 0)
C_RED = Color(255, 0, 0)
C_GLD = Color(255, 200, 0)
C_SLV = Color(200, 200, 255)


dev_mode: bool = False

shapes = [0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1,
          0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0]

brights = []
brights_lock = threading.Lock()

sparkles = []
sparkles_lock = threading.Lock()

gamma_lut = []
gamma_lut_lock = threading.Lock()

mqtt = mqttc.Client(socket. gethostname())
mqtt_queue = Queue()

conf_ws2812 = {
    "on_threshold": "20",
    "bright_var_rng": "64",
    "update_rate_hz": "100.0",
    "rotation_rate_hz": "10.0",
    "rotation_angle": "1.0",
    "rotation_angle_scaler": "5.0",
    "sparkle_trigger_on": "1,6,10",
    "sparkle_duration_ms": "150",
    "sparkle_brightness": "255"
}
conf_file = "./ws2812.conf.json"
conf_base_topic = "balcony/ws2812/"
conf_lock = threading.Lock()


def sig_handler(sig, frame):
    terminate(sig)


def terminate(sig):
    print(f"received signal {sig} - terminating...")
    stop()
    mqtt.loop_stop()
    save_conf()
    print("good-bye...")
    os._exit(9)


class PixelPainter(threading.Thread):

    def __init__(self, *strips):
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()
        self.strips = strips
        init_pixels(*self.strips)

    def run(self):
        while not self.stop_event.is_set():

            conf_lock.acquire()
            upd_rate = float(conf_ws2812["update_rate_hz"].replace(",", "."))
            conf_lock.release()

            set_pixels(*self.strips)

            time.sleep(1.0/upd_rate)


class BrightnessRotator(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.stop_event = threading.Event()
        init_brights()

    def run(self):

        deg = 0
        while not self.stop_event.is_set():
            calc_brightness(deg)

            conf_lock.acquire()
            rot_ang = float(conf_ws2812["rotation_angle"].replace(",", "."))
            rot_rate = float(conf_ws2812["rotation_rate_hz"].replace(",", "."))
            conf_lock.release()

            time.sleep(1.0/rot_rate)
            deg += rot_ang
            deg %= 360.0


class RandomSparkler(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        init_sparkles()

    def run(self):
        conf_lock.acquire()
        upd_rate = float(conf_ws2812["update_rate_hz"].replace(",", "."))
        sprk_dur = float(conf_ws2812["sparkle_duration_ms"].replace(
            ",", "."))/1000.0*upd_rate
        conf_lock.release()

        sparkle_count = random.randrange(50, 100)
        for i in range(sparkle_count):
            sparkle_idx = random.randrange(len(shapes))
            sparkle_count -= 1
            sparkles_lock.acquire()
            sparkles[sparkle_idx] = int(sprk_dur)
            sparkles_lock.release()
            time.sleep(sprk_dur/500.0)


ws2812_running = False
painter: PixelPainter = None
rotator: BrightnessRotator = None


def create_gamma_lut():
    gamma_lut_lock.acquire()
    gamma_lut.append(0)
    for i in range(1, 256):
        gamma_lut.append(int(255.0 ** ((i)/255.0)))
    gamma_lut_lock.release()


def init_brights():
    brights_lock.acquire()
    for i in range(len(shapes)):
        brights.append(0)
    brights_lock.release()
    create_gamma_lut()


def init_sparkles():
    sparkles_lock.acquire()
    for i in range(len(shapes)):
        sparkles.append(0)
    sparkles_lock.release()


def calc_brightness(start):

    conf_lock.acquire()
    rng = int(conf_ws2812["bright_var_rng"])
    conf_lock.release()

    cnt = len(shapes)
    start %= 360

    brights_lock.acquire()
    for i in range(cnt):
        deg = (360/cnt*i)+start
        base = 255 - rng
        val = (rng/2) * (1 + math.sin(math.radians(deg)))
        brights[i] = gamma_lut[int(base+val)] if shapes[i] == 0 else 255
    brights_lock.release()


def clear_pixels(*strips: PixelStrip):
    for strip in strips:
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, Color(0, 0, 0))
        strip.show()


def set_pixels(*strips: PixelStrip):

    sparkles_lock.acquire()
    sprk_brght = int(conf_ws2812["sparkle_brightness"])
    sparkles_lock.release()

    brights_lock.acquire()
    for strip in strips:
        for i in range(strip.numPixels()):
            if sparkles[i] > 0:
                sparkles[i] -= 1
                strip.setPixelColorRGB(
                    i, sprk_brght, sprk_brght, int(sprk_brght*0.5))
            elif shapes[i] == 1:
                strip.setPixelColorRGB(i, brights[i], 0, 0)
            else:
                strip.setPixelColorRGB(
                    i, 0, brights[i], int((255-brights[i])/5))
        strip.show()
    brights_lock.release()


def init_pixels(*strips: PixelStrip):
    for strip in strips:
        strip.begin()
    clear_pixels(*strips)


def load_conf() -> dict:
    try:
        global conf_ws2812
        with open(conf_file, "r") as cf:
            conf_ws2812 = json.load(cf)
            cf.close()
    except Exception:
        pass


def save_conf():
    try:
        with open(conf_file, "w") as cf:
            json.dump(conf_ws2812, cf, indent=4, ensure_ascii=True)
            cf.close()
    except Exception:
        pass


def mqtt_callback(client: mqttc.Client, userdata, message: mqttc.MQTTMessage):
    mqtt_queue.put(message)


def init_mqtt():
    mqtt.on_message = mqtt_callback
    mqtt.connect("infrapi")

    # subscribe to configuration topics
    mqtt.subscribe(conf_base_topic+"#")

    # subscribe to control topics
    mqtt.subscribe("balcony/weather/light/photo/percent")
    mqtt.subscribe("balcony/weather/wind/speed/bft")
    mqtt.subscribe("balcony/weather/wind/dir/idx")


def handle_mqtt(message: mqttc.MQTTMessage):
    t = message.topic.split('/')[-1]
    v = str(message.payload.decode('utf-8'))

    # print(f"{t} = {v}")

    if conf_base_topic in message.topic:
        conf_lock.acquire()
        conf_ws2812[t] = v
        if t == "rotation_angle_scaler":
            scaler = float(
                conf_ws2812["rotation_angle_scaler"].replace(",", "."))
            conf_ws2812["rotation_angle"] = str((float(v)+1.0)/scaler)
        conf_lock.release()
    elif t == "percent" and not dev_mode:
        if int(v) < int(conf_ws2812["on_threshold"]):
            start()
        else:
            stop()
    elif t == "bft":
        conf_lock.acquire()
        scaler = float(conf_ws2812["rotation_angle_scaler"].replace(",", "."))
        conf_ws2812["rotation_angle"] = str((float(v)+1.0)/scaler)
        conf_lock.release()
    elif t == "idx":
        conf_lock.acquire()
        triggers = list(map(int, conf_ws2812["sparkle_trigger_on"].split(",")))
        conf_lock.release()
        if int(v) in triggers:
            t = RandomSparkler()
            t.start()
            t.join()


def start():
    global ws2812_running
    global painter
    global rotator

    if not ws2812_running:
        painter = PixelPainter(*strips)
        rotator = BrightnessRotator()
        sparkler = RandomSparkler()
        painter.start()
        rotator.start()
        ws2812_running = True


def stop():
    global ws2812_running
    global painter
    global rotator
    global sparkler

    if ws2812_running:
        painter.stop_event.set()
        rotator.stop_event.set()
        rotator.join()
        painter.join()
        clear_pixels(*strips)
        ws2812_running = False


# Main program logic follows:
if __name__ == '__main__':

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--reverse',
                        action='store_true', help='reverse RGB to GRB')
    parser.add_argument('-d', '--development',
                        action='store_true', help='run in development mode')
    args = parser.parse_args()

    dev_mode = args.development

    random.seed()

    load_conf()

    strips = (
        PixelStrip(len(shapes), LED_PIN_1, LED_FREQ_HZ, LED_DMA,
                   LED_INVERT, LED_BRIGHTNESS, LED_CHN_1, 0 if args.reverse else None),

        PixelStrip(len(shapes), LED_PIN_2, LED_FREQ_HZ, LED_DMA,
                   LED_INVERT, LED_BRIGHTNESS, LED_CHN_2, 0 if args.reverse else None)
    )

    try:
        start()
        init_mqtt()
        mqtt.loop_start()

        while True:
            if not mqtt_queue.empty():
                handle_mqtt(mqtt_queue.get())
            time.sleep(.05)

    except Exception as e:
        terminate(e)
