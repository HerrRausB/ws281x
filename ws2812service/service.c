#include "service.h"

ws2811_return_t runService (svc_callback_t ptrCallback)
{
    uint32_t color = 0;
    int server_socket, 
        client_socket, 
        read_size;
    socklen_t socket_len;
    uint8_t message[WS2812_MSG_LEN], buf[256];
	struct sockaddr_in server;
    struct sockaddr client;
	
    fprintf (stderr, "started as service...\n");

 	if ((server_socket = socket(AF_INET , SOCK_STREAM , 0)) != -1)
    {
        setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
        fprintf (stderr, "server socket created...\n");
        //Prepare the sockaddr_in structure
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = htonl(INADDR_ANY);
        server.sin_port = htons (WS2812_PORT);
        // server.sin_port = getservbyname("ws2812","tcp")->s_port;

 	    // bind socket
        if (!bind(server_socket, (struct sockaddr*)&server , sizeof(server)))
        {	
            fprintf (stderr, "server socket bound...\n");
            listen(server_socket, 5);
            fprintf(stderr,"server created [fd = %d] - listening...\n", server_socket);
            do
            {
                socket_len = sizeof( (struct sockaddr *) &client);
                if ((client_socket = accept(server_socket, &client, &socket_len)) >= 0)
                {
                    fprintf(stderr,"connection accepted [fd = %d]\n", client_socket);
                    bzero (message,sizeof(message));
                    if (((read_size = recv(client_socket , message , WS2812_MSG_LEN , 0)) > 0) && ptrCallback)
                    {
						fprintf (stderr, "%d bytes received [%s]\n", read_size, message);

						if (strchr ("?", message[0]))
						{
							sprintf (buf, "%06X\n", color);
							write (client_socket, buf, strlen(buf));
						}
						else if (strchr ("vV", message[0]))
						{
							sprintf (buf, "WS2812 service last compiled at %s %s\n", __DATE__, __TIME__);
							write (client_socket, buf, strlen(buf));
						}
						else if (strchr ("qQxX", message[0]))
						{
							read_size = 0;
							ptrCallback (0);							
							fprintf(stderr,"Recieved '%c' - terminating...\n", message[0]);
						}
						else
						{
							color = (uint32_t)strtol(message, NULL, 16);
							fprintf(stderr,"setColor(0x%06X)\n", color);
							ptrCallback (color);
						}
                    }
                    close (client_socket);
                }
            }
            while (read_size);
        }
        close (server_socket);
    }

    fprintf (stderr, "last error: %d\n", errno);
    return 0;
}