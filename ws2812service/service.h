#ifndef __SERVICE_H__
#define __SERVICE_H__

#include <errno.h>
#include <fcntl.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <ws2811.h>

#define WS2812_PORT     8888
#define WS2812_MSG_LEN  10 // strlen("0xFFFFFF") + 2

typedef ws2811_return_t (*svc_callback_t)(uint32_t color);

ws2811_return_t runService (svc_callback_t ptrCallback);

#endif